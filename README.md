
# Cloudflare TODO app

This is an implementation of a simple Cloudflare TODO list application, created for the master couse "Software Architecture in Practice" at AU.

## Installation 

Install wrangler (requires node.js) by running

```bash
npm install wrangler -g
```

Login to Cloudflare Workers and follow the login process

```bash
wrangler login
```

**Worker setup**

To get started fill out a `wrangler.toml` file, here is an example of such file

```toml 
name = "todo-app"
type = "webpack"

account_id = "INSERT_ACCOUNT_ID"
workers_dev = true
route = ""
zone_id = ""

kv_namespaces = [ 
	{ binding = "TODO", preview_id = "INSERT_WORKER_KV_PREVIEW_ID", id = "INSERT_WORKER_KV_ID" }
]
```

Change the value of `INSERT_ACCOUNT_ID` with the `account_id` returned by `wrangler login` or found by running

```bash
wrangler whoami
```

**Workers KV setup**

Create a new key-value store by running

```bash
wrangler kv:namespace create "TODO"
```

Replace the line below in the `wrangler.toml`, with the line returned by `wrangler kv:namespace create "TODO"`

```bash 
{ binding = "TODO", preview_id = "INSERT_WORKER_KV_PREVIEW_ID", id = "INSERT_WORKER_KV_ID" }
```

To create a preivew_id run:

```bash
wrangler kv:namespace create "TODO" --preview
```

Replace the `INSERT_WORKER_KV_PREVIEW_ID` with the `preview_id` and replace the `INSERT_WORKER_KV_ID` with the `id`. 

## Run Locally

Clone the project

```bash
git clone https://bitbucket.org/saipgolf/todo-app-cloudflare
```

Install dependencies

```bash
npm install wrangler -g
```

Start

```bash
wrangler dev
```

## Deployment

To deploy this project run

```bash
wrangler publish
``` 