addEventListener('fetch', event => {
  event.respondWith(handleRequest(event.request))
})
/**
 * Respond with hello worker text
 * @param {Request} request
 */
async function handleRequest(request) {
  return new Response('inner-about: Sample application created by Team Golf', {
    headers: { 'content-type': 'text/plain; charset=utf-8' },
  })
}
