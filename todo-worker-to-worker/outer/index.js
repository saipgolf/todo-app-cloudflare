const Router = require('./router')

addEventListener('fetch', event => {
    event.respondWith(handleRequest(event.request))
})

async function todoAdd(request) {
  var body = null;
  try {
    body = await request.json();
  } catch (error) {
    return new Response("request is not defined", {
      status: 400
    });
  }

  let user = body.user;
  if (!body.user) {
    return new Response("user is not defined", {
      status: 400
    });
  }

  let todo = body.todo;
  if (!todo)
  return new Response("todo is not defined", {
    status: 400
  });

  let data = JSON.parse(await TODO.get(user));
  if (data === null) {
    data = [ todo ];
  }
  else {
    data.push(todo);
  }
  
  await TODO.put(user, JSON.stringify(data));
  return new Response();
}

async function todoList(request) {

  const queryData = Url.parse(request.url, true).query;
  const user = queryData.user;

  if (!user) {
    return new Response("query param user is not defined", {
      status: 400
    });
  }

  var data = JSON.parse(await TODO.get(user));

  return json(data)
}

function json(data) {
  var response = new Response(JSON.stringify(data), {
    headers: { "Content-Type": "application/json" }
  });
  return response;
}

async function todoClear(request) {
  var body = null;
  try {
    body = await request.json();
  } catch (error) {
    return new Response("request is not defined", {
      status: 400
    });
  }
  
  let user = body.user;
  if (!body.user) {
    return new Response("user is not defined", {
      status: 400
    });
  }
  
  await TODO.delete(user);

  return new Response();
}

async function handleRequest(request) {
    const r = new Router()
    await r.get('/about', async () => await fetch('https://todo-worker-to-worker-inner.saip.workers.dev'))
    await r.get('/credit', () => new Response('Created by Team Golf', {
      headers: { 'content-type': 'text/plain; charset=utf-8' },
    }))
    await r.post('/todo/add', async request => await todoAdd(request))
    await r.get('/todo', async request => await todoList(request))
    await r.delete('/todo/clear', async request => await todoClear(request))

    const resp = await r.route(request)
    return resp
}